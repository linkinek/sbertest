package com.linkinek.yandextest.utils;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import ru.sbtqa.tag.pagefactory.Page;

import java.util.List;

public class Utils {

    public static WebElement filterByName(List<WebElement> webElementList, String name) {

        WebElement webElement = webElementList.stream().filter(element -> element.getText().equals(name)).findFirst().orElse(null);
        if (webElement == null) {
            Assert.fail("Element by name '" + name + "' not found");
        }
        return webElement;
    }

    public static void waitElementIsNotDisplayed(WebElement webElement) throws InterruptedException {
        waitElementByFlagDisplayed(webElement, false);
    }

    public static void waitElementIsDisplayed(WebElement webElement) throws InterruptedException {
        waitElementByFlagDisplayed(webElement, true);
    }

    public static void waitElementIsNotExists(WebElement webElement) throws InterruptedException {
        waitElementByFlagExists(webElement, false);
    }

    public static void waitElementIsExists(WebElement webElement) throws InterruptedException {
        waitElementByFlagExists(webElement, true);
    }

    public static void waitElementIsExistsThenNotExists(WebElement webElement) throws InterruptedException {
        waitElementByFlagExists(webElement, true);
        waitElementByFlagExists(webElement, false);
    }

    private static void waitElementByFlagDisplayed(WebElement webElement, boolean displayed) throws InterruptedException {
        long finishTime = System.currentTimeMillis() + 10000;
        boolean success = false;
        while (finishTime > System.currentTimeMillis()) {
            if (webElement.isDisplayed() == displayed) {
                Thread.sleep(100);
            } else {
                success = true;
                break;
            }
        }
        if (!success) {
            Assert.fail("The item was" + (displayed ? " visible" : " not visible") + " for 10 seconds: " + webElement);
        }
    }

    private static void waitElementByFlagExists(WebElement webElement, boolean exists) throws InterruptedException {
        long finishTime = System.currentTimeMillis() + 10000;
        boolean success = false;
        while (finishTime > System.currentTimeMillis()) {
            try {
                webElement.getTagName();
                if (exists) {
                    success = true;
                    break;
                }
            } catch (Exception ignored) {
                if (exists) {
                    Thread.sleep(100);
                } else {
                    success = true;
                    break;
                }
            }
        }
        if (!success) {
            Assert.fail("The item was" + (exists ? " exists" : " not exists") + " for 10 seconds: " + webElement);
        }
    }

    public static void findAndClickElementIfDisplayed(Page page, String name) {
        WebElement element;
        try {
            element = page.getElementByTitle(name);
        } catch (Exception ignored) {
            return;
        }
        if (element.isDisplayed()) {
            element.click();
        }
    }
}
