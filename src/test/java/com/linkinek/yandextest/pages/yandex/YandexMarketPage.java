package com.linkinek.yandextest.pages.yandex;

import com.linkinek.yandextest.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;

import java.util.List;

import static com.linkinek.yandextest.utils.Utils.*;
import static org.junit.Assert.assertEquals;

@PageEntry(title = "Яндекс Маркет")
public class YandexMarketPage extends Page {

    private String titleFirstItem;
    private static final By locatorTitleForCard = By.xpath("descendant::div[contains(@class, 'n-snippet-card2__title')]");

    @ElementTitle("Список меню")
    @FindBy(xpath = "//a[contains(@class, 'topmenu__link')]")
    public List<WebElement> menuList;

    @ElementTitle("Список левого меню")
    @FindBy(xpath = "//a[contains(@class, 'catalog-menu__list-item')]")
    public List<WebElement> leftMenuList;

    @ElementTitle("Фильтр по цене от")
    @FindBy(xpath = "//span[contains(@class, 'input_price_from')]/descendant::input")
    public WebElement priceFilterFrom;

    @ElementTitle("Фильтр по цене до")
    @FindBy(xpath = "//span[contains(@class, 'input_price_to')]/descendant::input")
    public WebElement priceFilterTo;

    @ElementTitle("Список чекбоксов производителя")
    @FindBy(xpath = "//a[contains(@class, 'n-filter-block__item')]")
    public List<WebElement> checkboxList;

    @ElementTitle("Лоадинг")
    @FindBy(xpath = "//div[contains(@class, 'preloadable__preloader')]")
    public WebElement loading;

    @ElementTitle("Список товаров")
    @FindBy(xpath = "//div[contains(@class, 'n-snippet-list')]/div[contains(@class, 'n-snippet-card2')]")
    public List<WebElement> itemList;

    @ElementTitle("Поиск")
    @FindBy(id = "header-search")
    public WebElement search;

    @ElementTitle("Имя товара")
    @FindBy(xpath = "//h1[contains(@class, 'title')]")
    public WebElement titleItem;

    public YandexMarketPage() {
        PageFactory.initElements(PageFactory.getDriver(), this);
    }

    @ActionTitle("кликает по меню")
    public void clickToMenu(String name) {
        clickWebElement(Utils.filterByName(menuList, name));
    }

    @ActionTitle("кликает по левому меню")
    public void clickToLeftMenu(String name) {
        clickWebElement(Utils.filterByName(leftMenuList, name));
    }

    @ActionTitle("фильтрует по цене от")
    public void setPriceFilterFrom(String value) {
        fillField(priceFilterFrom, value);
    }

    @ActionTitle("фильтрует по цене до")
    public void setPriceFilterTo(String value) {
        fillField(priceFilterTo, value);
    }

    @ActionTitle("фильтрует по производителю")
    public void setFilterByProducer(String name) throws InterruptedException {
        setCheckBoxState(Utils.filterByName(checkboxList, name), true);
        waitElementIsExistsThenNotExists(loading);
    }

    @ActionTitle("кликает по кнопке")
    public void clickButton(String name) {
        findAndClickElementIfDisplayed(this, name);
    }

    @ActionTitle("проверяет количество товаров")
    public void checkCountItems(String count) {
        assertEquals(itemList.size(), Integer.valueOf(count).intValue());
    }

    @ActionTitle("запоминает название первого товара")
    public void saveNameFirstItem() {
        titleFirstItem = itemList.get(0).findElement(locatorTitleForCard).getText();
    }

    @ActionTitle("вводит в поисковую строку запомненное название")
    public void searchNameFirstItem() {
        search.clear();
        search.sendKeys(titleFirstItem);
        search.sendKeys(Keys.ENTER);
    }

    @ActionTitle("проверяет название товара")
    public void checkItemName() {
        assertEquals(titleFirstItem, titleItem.getText());
    }
}
