package com.linkinek.yandextest.pages.yandex;

import com.linkinek.yandextest.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;

import java.util.List;

@PageEntry(title = "Яндекс")
public class YandexPage extends Page {

    @ElementTitle("Список таб")
    @FindBy(xpath = "//a[contains(@class, 'home-tabs__link')]")
    public List<WebElement> tabList;

    public YandexPage() {
        PageFactory.initElements(PageFactory.getDriver(), this);
    }

    @ActionTitle("кликает по табе")
    public void clickToTab(final String name) {
        clickWebElement(Utils.filterByName(tabList, name));
    }
}
